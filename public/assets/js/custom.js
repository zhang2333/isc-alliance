(function() {

	"use strict";
  
	var app = {
		
		init: function() {

			//=== Main visible ===\\
			this.mainVisible();

			//=== lazy loading effect ===\\
			this.lazyLoading();

			this.setUpListeners();

			//=== Custom scripts ===\\
			this.btnHover();
			this.appendMfBg();
			this.appendBtnTop();
			this.formingHrefTel();
			this.contentTable();
			this.detectIE();

			//=== Plugins ===\\
			this.device();
			this.scrollToFixed();
			this.forms();
			this.spincrement();

		},
 
		setUpListeners: function() {

			//=== Ripple effect for buttons ===\\
			$(".ripple").on("click", this.btnRipple);

			//=== Header lang ===\\
			// Header lang open
			$(".header-lang-current").on("click", this.headerLangOpen);
			// Header lang close not on this element \\
			$(document).on("click", this.headerLangCloseNotEl);

			//=== Header mobile/tablet navbar ===\\
			// Header navbar toggle \\
			$(".header-navbar-btn").on("click", this.headerNavbarToggle);
			// Header navbar close not on this element \\
			$(document).on("click", this.headerNavbarNotEl);

			//=== Mobile/tablet main menu ===\\
			// Main menu toogle \\
			$(".main-mnu-btn").on("click", this.MainMenuToggle);
			// Main menu submenu toogle \\
			$(".mmm-btn").on("click", this.MainMenuSubmenuToggle);
			// Main menu close not on this element \\
			$(document).on("click", this.MainMenuCloseNotEl);

			//=== Side toggle ===\\
			$(".side-open").on("click", this.sideOpen);
			$(document).on("click", ".side-close, .side-visible", this.sideClose);

			//=== Tab ===\\
			$(".tabs-nav li").on("click", this.tab);

			//=== Accordion ===\\
			$(".accordion-trigger").on("click", this.accordion);
			
			//=== Form field ===\\
			$(".form-field").each(this.inputEach);
			$(".form-field-input")
				.on("focus", this.inputFocus)
				.on("keyup change", this.inputKeyup)
				.on("blur", this.inputBlur);

			//=== Button top ===\\
			$(document).on("click", '.btn-top', this.btnTop);
			$(window).on("scroll", this.btnTopScroll);
			
		},

		//=== Body visible ===\\
		mainVisible: function() {

			$(".main").addClass("main-visible");

		},

		appendMfBg: function() {

			$("body").append('<div class="mf-bg"></div>');

		},

		appendBtnTop: function() {

			$("body").append('<div class="btn-top"><svg class="btn-icon-right" viewBox="0 0 13 9" width="13" height="9"><use xlink:href="assets/img/sprite.svg#arrow-right"></use></svg></div>');

		},

		btnTop: function() {
			
			$('html, body').animate({scrollTop: 0},1000, function() {
				$(this).removeClass("active");
			});

		},

		btnTopScroll: function() {
			
			var btnTop = $('.btn-top');
			
			if ($(this).scrollTop() > 700) {

				btnTop.addClass("active");

			} else {

				btnTop.removeClass("active");
				
			}

		},

		//=== Tab ===\\
		tab: function() {

			var _this = $(this),
				index = _this.index(),
				tabs = _this.closest(".tabs"),
				items = tabs.find(".tabs-item");

			if (!_this.hasClass("active")) {

				items
					.eq(index)
					.add(_this)
					.addClass("active")
					.siblings()
					.removeClass("active");
			
			}

		},

		//=== Accordion ===\\
		accordion: function(e) {

			e.originalEvent.preventDefault();

			var _this = $(this),
				item = _this.closest(".accordion-item"),
				container = _this.closest(".accordion"),
				items = container.find(".accordion-item"),
				content = item.find(".accordion-content"),
				otherContents = container.find(".accordion-content"),
				duration = 300;

			if (!item.hasClass("active")) {
				items.removeClass("active");
				item.addClass("active");
				otherContents.stop(true, true).slideUp(duration);
				content.stop(true, true).slideDown(duration);
			} else {
				content.stop(true, true).slideUp(duration);
				item.removeClass("active");
			}

		},
		
		//=== Header lang ===\\
		headerLangOpen: function() {

			$(this).parent().toggleClass("open");

		},
		headerLangCloseNotEl: function(e) {
			
			if($(".header-lang").hasClass("open")) {
				if ($(e.originalEvent.target).closest(".header-lang").length) return;
				$(".header-lang").removeClass("open");
				e.originalEvent.stopPropagation();
			}

		},

		//=== Mobile/tablet main menu ===\\
		MainMenuToggle: function() {

			var _this = $(this),
				_body = $("body"),
				headerH = _this.closest(".header").outerHeight(),
				mnu = $(".mmm"),
				offsetTop = $(".header-fixed").offset().top;
				
			mnu.css("padding-top", headerH);
			$(this).toggleClass("active");

			_body.toggleClass("mmm-open").scrollTop(offsetTop);
				
			if(_body.hasClass("mmm-open")) {
				$(".mf-bg").addClass("visible mm");
			} else {
				$(".mf-bg").removeClass("visible mm");
			}

		},
		MainMenuSubmenuToggle: function() {

			var _this = $(this),
				item = _this.parent(),
				content = item.find(".mmsm");

			item.toggleClass("open");
			content.slideToggle();

		},
		MainMenuCloseNotEl: function(e) {

			if($("body").hasClass("mmm-open")) {
				if ($(e.originalEvent.target).closest(".mmm, .main-mnu-btn").length) return;
				$("body").removeClass("mmm-open");
				$(".main-mnu-btn").removeClass("active");
				$(".mf-bg").removeClass("visible mm");
				e.originalEvent.stopPropagation();
			}

		},

		//=== Header mobile/tablet navbar ===\\
		headerNavbarToggle: function() {

			$(this).parent().toggleClass("open");

		},
		headerNavbarNotEl: function(e) {

			if ($(e.originalEvent.target).closest(".header-navbar").length) return;
			$(".header-navbar").removeClass("open");
			e.originalEvent.stopPropagation();

		},

		//=== Side toggle ===\\
		sideOpen: function(e) {

			e.originalEvent.preventDefault();

			var side = $($(this).attr("data-side"));

			if(side.length) {

				side.toggleClass("open");
				if(!e.currentTarget.classList.contains("panel-settings-btn")) {
					$(".mf-bg").toggleClass("visible side-visible");
				}

			}

		},
		sideClose: function() {

			$(".side, .sidebar-filters").removeClass("open");
			$(".mf-bg").removeClass("visible side-visible");

		},

		//=== Form input ===\\
		inputEach: function() {

			var _this = $(this),
				val = _this.find(".form-field-input").val();

			if (val === "") {
				_this.removeClass("focus");
			} else {
				_this.addClass("focus");
			}

		},
		inputFocus: function() {

			var _this = $(this),
				wrappInput = _this.parent();

			wrappInput.addClass("focus");

		},
		inputKeyup: function() {

			var _this = $(this),
				val = _this.val(),
				wrappInput = _this.parent();

			if (val === "" && !_this.is(":focus")) {
				wrappInput.removeClass("focus");
			} else {
				wrappInput.addClass("focus");
			}

		},
		inputBlur: function() {

			var _this = $(this),
				val = _this.val(),
				wrappInput = _this.parent();

			if(val === "") {
				wrappInput.removeClass("focus"); 
			}

		},

		//=== Ripple effect for buttons ===\\
		btnRipple: function(e) {
			
			var _this = $(this),
				offset = $(this).offset(),
				positionX = e.originalEvent.pageX - offset.left,
				positionY = e.originalEvent.pageY - offset.top;
			_this.append("<div class='ripple-effect'>");
			_this
				.find(".ripple-effect")
				.css({
					left: positionX,
					top: positionY
				})
				.animate({
					opacity: 0
				}, 1500, function() {
					$(this).remove();
				});

		},

		btnHover: function() {

			var btns = document.querySelectorAll(".btn, .el-ripple"),
				btn = [];

			btns.forEach(function(element, index) {

				var span = document.createElement("span"); 
				span.className = "el-ripple-circle";
				element.appendChild(span);

				// If The span element for this element does not exist in the array, add it.
				if (!btn[index])
				btn[index] = element.querySelector(".el-ripple-circle");

				element.addEventListener("mouseenter", function(e) {	
					btnHandler(element, index, e);			
				});

				element.addEventListener("mouseleave", function(e) {
					btnHandler(element, index, e);
				});
				
			});

			const btnHandler = function(element, index, e) {

				let offset = element.getBoundingClientRect(),
					left = e.pageX - offset.left - window.scrollX,
					top = e.pageY - offset.top - window.scrollY;

				btn[index].style.left = left + "px";
				btn[index].style.top = top + "px";

			}

		},

		//=== Forming href for phone ===\\
		formingHrefTel: function() {

			var linkAll = $('.formingHrefTel'),
				joinNumbToStringTel = 'tel:';

			$.each(linkAll, function () {
				var _this = $(this),
					linkValue = _this.text(),
					arrayString = linkValue.split("");

				for (var i = 0; i < arrayString.length; i++) {
					var thisNunb = app.isNumber(arrayString[i]);
					if (thisNunb === true || (arrayString[i] === "+" && i === 0)) {
						joinNumbToStringTel += arrayString[i];
					}
				}

				_this.attr("href", function () {
					return joinNumbToStringTel;
				});
				joinNumbToStringTel = 'tel:'

			});

		},

		isNumber: function(n) {

			return !isNaN(parseFloat(n)) && isFinite(n);

		},
		
		//=== Content table responsive ===\\
		contentTable: function() {

			var contentTable = $(".content");
			if(contentTable.length) {
				
				$.each(contentTable.find("table"), function() {
					$(this).wrap("<div class='table-responsive-outer'></div>").wrap("<div class='table-responsive'></div>");
				});
				
			}

		},

		//=== Plugins ===\\

		lazyLoading: function() {

			var observer = lozad('.lazy');
			observer.observe();

		},

		device: function() {

			if( (device.mobile() || device.tablet()) && device.ios() ) {
				var tempCSS = $('a').css('-webkit-tap-highlight-color');
				$('main, .main-inner').css('cursor', 'pointer')
						 .css('-webkit-tap-highlight-color', 'rgba(0, 0, 0, 0)');
				$('a').css('-webkit-tap-highlight-color', tempCSS);
			}

		},

		scrollToFixed: function() {

			if($('.header-fixed').length) {

				$('.header-fixed').scrollToFixed({
					preFixed: function() { $(this).addClass("fixed"); },
					postFixed: function() { $(this).removeClass("fixed"); }
				});

			}
			
		},

		forms: function() {

			$.validator.addMethod("customemail", function (value, element) {
				return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
			},
				"The email is not a valid email."
			);
			

			$(".contact-form").validate({
				rules: {
					ContactName: {
					required: true,
					minlength: 2
					},
					ContactPhone: {
					required: true
					},
					ContactEmail: {
						required: true,
						email: true,
						customemail: true
					},
				},
				messages: {
					ContactName: {
					required: "The name field is required.",
					},
					ContactPhone: {
					required: "The phone field is required.",
					},
					ContactEmail: {
						required: "The email field is required.",
						email: "The email field is required.",
						customemail: "The email is not a valid email."
					},
				},
				submitHandler: function(form) {

					$(form).trigger("reset");
					$(".form-field").removeClass("focus");
			
					alert('Successfully sent!');
		
				}
			});
			
			$(".footer-subscribe").validate({
				rules: {
					ContactEmail: {
						required: true,
						email: true,
						customemail: true
					},
				},
				messages: {
					ContactEmail: {
						required: "The email field is required.",
						email: "The email field is required.",
						customemail: "The email is not a valid email."
					},
				},
				submitHandler: function(form) {

					$(form).trigger("reset");
					$(".form-field").removeClass("focus");
			
					alert('Successfully sent!');
		
				}
			});

			$(".order-form").validate({
				rules: {
					orderName: {
					required: true,
					minlength: 2
					},
					orderPhone: {
					required: true
					}
				},
				messages: {
					orderName: {
					required: "The name field is required.",
					},
					orderPhone: {
					required: "The phone field is required.",
					}
				},
				submitHandler: function(form) {

					$(form).trigger("reset");
					$(".form-field").removeClass("focus");
			
					alert('Successfully sent!');
		
				}
			});

		},

		spincrement: function() {

			var show = true;
			var countbox = ".spincrement-container";

			if($(countbox).length) {
			
				$(window).on("scroll load resize", function () {
					if (!show) return false;
					var w_top = $(window).scrollTop();
					var e_top = $(countbox).offset().top;
					var w_height = $(window).height();
					var d_height = $(document).height();
					var e_height = $(countbox).outerHeight();
					if (w_top + 500 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
						$('.spincrement').spincrement({
							duration: 1500,
							leeway: 10
						});
					show = false;
					}
				});
			}

		},

		//=== detect IE ===\\
		detectIE: function() {

			if(this.detectIECheck()) {
				var body = document.querySelector("body"),
					msg = 'Unfortunately, the browser Internet Explorer you use is outdated and cannot display the site normally. <br> Please open the site in another browser';
				body.classList.add("overflow-hidden");
				body.innerHTML = '<div class="ie-browser"><div class="ie-browser-tr"><div class="ie-browser-td">'+ msg +'</div></div></div>';
			}

		},
		detectIECheck: function() {

			var ua = window.navigator.userAgent;
			  
			var msie = ua.indexOf('MSIE ');
			if (msie > 0) {
				// IE 10 or older => return version number
				return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			}
			  
			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}
			  
			// other browser
			return false;

		}
		
	}
 
	app.init();
 
}());
document.addEventListener('DOMContentLoaded', function() {
	// 初始化存储
	if (!localStorage.getItem('iscApplications')) {
		localStorage.setItem('iscApplications', JSON.stringify([]));
	}

	// 获取申请按钮
	const applyBtn = document.querySelector('.wrap-btn.intro-btns a');

	// 处理申请按钮点击事件
	applyBtn.addEventListener('click', function(e) {
		e.preventDefault();

		// 检查是否已存在表单
		const existingForm = document.querySelector('.application-form');
		if (existingForm) {
			existingForm.remove();
			return;
		}

		// 获取已申请列表
		const applications = JSON.parse(localStorage.getItem('iscApplications') || '[]');

		// 创建申请表单
		const formHtml = `
            <div class="application-form" style="margin: 20px; padding: 20px; background: #f5f5f5; border-radius: 5px;">
                <div style="display: flex; justify-content: space-between; align-items: center; margin-bottom: 15px;">
                    <h3 style="margin: 0;">生成ISC认证</h3>
                    <button onclick="closeForm()" style="background: none; border: none; font-size: 20px; cursor: pointer;">&times;</button>
                </div>
                <div style="margin-bottom: 10px;">
                    <input type="text" id="websiteUrl" placeholder="输入网址" style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ddd; border-radius: 4px;">
                    <input type="text" id="websiteName" placeholder="输入网站名称" style="width: 100%; padding: 8px; margin-bottom: 10px; border: 1px solid #ddd; border-radius: 4px;">
                    <button onclick="generateISC()" style="padding: 8px 15px; background: #007bff; color: white; border: none; border-radius: 3px; cursor: pointer; width: 100%;">生成认证代码</button>
                </div>
                <div id="generatedCode" style="display: none;">
                    <h4>生成的认证代码：</h4>
                    <pre style="background: #fff; padding: 15px; border-radius: 3px; white-space: pre-wrap; word-wrap: break-word;"></pre>
                    <button onclick="copyToClipboard('cert')" style="padding: 8px 15px; background: #28a745; color: white; border: none; border-radius: 3px; cursor: pointer; margin-bottom: 10px; width: 100%;">复制认证代码</button>
                    <hr style="margin: 15px 0;">
                    <h4>网站列表项代码：</h4>
                    <h5>请联系管理员将以下代码添加到网站列表中：</h5>
                    <pre style="background: #fff; padding: 15px; border-radius: 3px; white-space: pre-wrap; word-wrap: break-word;"></pre>
                    <button onclick="copyToClipboard('list')" style="padding: 8px 15px; background: #28a745; color: white; border: none; border-radius: 3px; cursor: pointer; width: 100%;">复制列表代码</button>
                </div>
                <div id="applicationsList" style="margin-top: 20px;">
                    <h4>你已申请的网站：</h4>
                    <div class="applications-container" style="max-height: 200px; overflow-y: auto; background: #fff; padding: 10px; border-radius: 3px;">
                        ${applications.map(app => `
                            <div style="padding: 8px; border-bottom: 1px solid #eee;">
                                <strong>${app.name}</strong><br>
                                <small>网址：${app.url}<br>编号：${app.certNumber}<br>申请时间：${app.applyTime}</small>
                            </div>
                        `).join('')}
                    </div>
                </div>
            </div>
        `;

		// 在按钮后插入表单
		this.insertAdjacentHTML('afterend', formHtml);

		// 添加关闭表单的函数
		window.closeForm = function() {
			document.querySelector('.application-form').remove();
		};

		// 添加生成认证码的函数
		window.generateISC = function() {
			const url = document.getElementById('websiteUrl').value;
			const name = document.getElementById('websiteName').value;

			if (!url || !name) {
				alert('请填写网址和网站名称！');
				return;
			}

			// 检查URL是否已经申请过
			const applications = JSON.parse(localStorage.getItem('iscApplications') || '[]');
			if (applications.some(app => app.url === url)) {
				alert('该网站已经申请过ISC认证！');
				return;
			}

			// 生成认证编号
			const dateStr = `${Date.now().toString().slice(-5).replace(/^0+/, '')}${Math.floor(Math.random() * 10000)}`; // Generate a 9-digit unique number based on the current timestamp and a random number // Generate a 9-digit unique number//Let the middle not be a date, so that he will not repeat it，Based on the timestamp and random number, a calculation is performed to obtain a number with more than ten digits.
			let maxNum = 0;
			applications.forEach(app => {
				if (app.certNumber.includes(dateStr)) {
					const num = parseInt(app.certNumber.split('-')[2]);
					if (num > maxNum) maxNum = num;
				}
			});
			const nextNum = String(maxNum + 1).padStart(2, '0');
			const certNumber = `认ISC-${dateStr}-${nextNum}`;
			// 生成认证代码
			const certCode = `<a href="${url}" style="text-decoration: none; color: rgb(20, 20, 20);">
    <img src="https://111111.eu.org/assets/img/ICS.svg" style="float: left; height: 23px; margin-right: 5px;" alt="ICS">
    <p class="no-wrap">${certNumber}</p>
</a>`;

			// 生成网站列表项代码
			const listItemCode = `<div class="col-lg-4 col-md-6 col-12 item">
    <a href="${url}" class="iitem item-style">
        <div class="iitem-icon">
            <i class="material-icons material-icons-outlined md-48">public</i>
        </div>
        <div class="iitem-icon-bg">
            <i class="material-icons material-icons-outlined">public</i>
        </div>
        <h3 class="iitem-heading item-heading-large">${name}&nbsp;</h3>
        <div class="iitem-desc">网址： ${url} 编号：${certNumber}&nbsp;</div>
    </a>
</div>`;

			// 保存申请记录
			applications.push({
				url: url,
				name: name,
				certNumber: certNumber,
				applyTime: new Date().toLocaleString(),
				appliedBy: 'zhang2333-Myself'
			});
			localStorage.setItem('iscApplications', JSON.stringify(applications));

			// 更新显示
			const generatedDiv = document.getElementById('generatedCode');
			generatedDiv.style.display = 'block';
			generatedDiv.querySelector('pre:first-of-type').textContent = certCode;
			generatedDiv.querySelector('pre:last-of-type').textContent = listItemCode;

			// 更新申请列表显示
			const applicationsContainer = document.querySelector('.applications-container');
			applicationsContainer.innerHTML = applications.map(app => `
                <div style="padding: 8px; border-bottom: 1px solid #eee;">
                    <strong>${app.name}</strong><br>
                    <small>网址：${app.url}<br>编号：${app.certNumber}<br>申请时间：${app.applyTime}</small>
                </div>
            `).join('');
		};

		// 添加复制功能
		window.copyToClipboard = function(type) {
			const element = type === 'cert' ?
				document.querySelector('#generatedCode pre:first-of-type') :
				document.querySelector('#generatedCode pre:last-of-type');

			const text = element.textContent;

			navigator.clipboard.writeText(text).then(() => {
				alert('代码已复制到剪贴板！');
			}).catch(err => {
				console.error('复制失败:', err);
				alert('复制失败，请手动复制代码。');
			});
		};
	});
});